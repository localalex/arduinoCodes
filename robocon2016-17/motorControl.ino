int motorLF = 8;
int motorLR = 9;
int motorRF = 10;
int motorRR = 11;

void setup()
{
  pinMode(motorLF,OUTPUT);
  pinMode(motorLR,OUTPUT);
  pinMode(motorRF,OUTPUT);
  pinMode(motorRR,OUTPUT);

}

void loop()
{
   digitalWrite(motorLF,HIGH);
   digitalWrite(motorLR,LOW);
   digitalWrite(motorRF,HIGH);
   digitalWrite(motorRR,LOW);

   delay(5000);

   digitalWrite(motorLF,LOW);
   digitalWrite(motorLR,HIGH);
   digitalWrite(motorRF,LOW);
   digitalWrite(motorRR,HIGH);

   delay(5000);


}
