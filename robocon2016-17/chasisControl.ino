
// Motors
int L1F = 2;
int L1R = 3;

int L2F = 4;
int L2R = 5;

int R1F = 6;
int R1R = 7;

int R2F = 8;
int R2R = 9;

//Enable Pins
int EN1 = 30;
int EN2 = 32;
int EN3 = 34;
int EN4 = 36;

void setup()
{
  pinMode(L1F,OUTPUT);
  pinMode(L1R,OUTPUT);
  pinMode(L2F,OUTPUT);
  pinMode(L2R,OUTPUT);
  pinMode(R1F,OUTPUT);
  pinMode(R1R,OUTPUT);
  pinMode(R2F,OUTPUT);
  pinMode(R2R,OUTPUT);

  pinMode(EN1,LOW);
  pinMode(EN2,LOW);
  pinMode(EN3,LOW);
  pinMode(EN4,LOW);



}


void loop()
{

// Left Front Side Motor Forward
  digitalWrite(EN1,HIGH);

  digitalWrite(L1F,HIGH);
  digitalWrite(L1R,LOW);
  delay(5000);
//Left Front Side Motor Reverse
  digitalWrite(L1F,LOW);
  digitalWrite(L1R,HIGH);
  delay(5000);
  digitalWrite(EN1,LOW);
//Left Rear Side Motor Forward

  digitalWrite(EN2,HIGH);

  digitalWrite(L2F,HIGH);
  digitalWrite(L2R,LOW);
  delay(5000);
  //Left Side rear Motor Reverse
  digitalWrite(L2F,LOW);
  digitalWrite(L2R,HIGH);
  delay(5000);
  digitalWrite(EN2,LOW);

//Right Front side Motor Forward
  digitalWrite(EN3,HIGH);

  digitalWrite(R1F,HIGH);
  digitalWrite(R1R,LOW);
  delay(5000);
//Right Front Side Motor Reverse
  digitalWrite(R1F,LOW);
  digitalWrite(R1R,HIGH);
  delay(5000);
digitalWrite(EN3,LOW);

  digitalWrite(EN4,HIGH);

  digitalWrite(R2F,HIGH);
  digitalWrite(R2R,LOW);
  delay(5000);
  digitalWrite(R2F,LOW);
  digitalWrite(R2R,HIGH);
  delay(5000);
  digitalWrite(EN4,LOW);

}
