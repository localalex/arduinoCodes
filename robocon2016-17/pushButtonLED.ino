/* Basic Digital Read
 * ------------------
 *
 * turns on and off a light emitting diode(LED) connected to digital
 * pin 13, when pressing a pushbutton attached to pin 7. It illustrates the
 * concept of Active-Low, which consists in connecting buttons using a
 * 1K to 10K pull-up resistor.
 *
 * Created 1 December 2005
 * copyleft 2005 DojoDave <http://www.0j0.org>
 * http://arduino.berlios.de
 *
 */

int ledPin = 13; // choose the pin for the LED
int inPin = 11;   // choose the input pin (for a pushbutton)
boolean val;     // variable for reading the pin status

void setup() {
  pinMode(ledPin, OUTPUT);  // declare LED as output
  pinMode(inPin, INPUT);    // declare pushbutton as input
  Serial.begin(57600);
}

void loop(){
  val = digitalRead(inPin);  // read input value
  Serial.println(val);
  if (val == LOW) {         // check if the input is HIGH (button released)
    digitalWrite(ledPin, LOW);  // turn LED OFF
//    Serial.println("LED OFF");
  }
  else if(val == HIGH)  {
    digitalWrite(ledPin, HIGH);  // turn LED ON
  //  Serial.println("LED ON");
  }
}
