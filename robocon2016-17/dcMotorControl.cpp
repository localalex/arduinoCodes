int motorPin1 = 6;
int motorPin2 = 7;

void setup()
{
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);

}

void loop()
{
  /* code */
  analogWrite(motorPin1, 100);
  analogWrite(motorPin2, 0);
  delay(5000);

  analogWrite(motorPin1,0);
  analogWrite(motorPin2,0);
  delay(5000);

  analogWrite(motorPin1,200);
  analogWrite(motorPin2,0);
  delay(5000);

  analogWrite(motorPin1,0);
  analogWrite(motorPin2,0);
  delay(5000);


}
