//Final Bot Code for bot including a 90 degree omni wheel setup.
// and throwing mechanism on the bot.


#include<Wire.h>
int frontMotorDirection = 25;
int fronMotorPWM = 5;

int leftMotorDirection = 24;
int leftMotorPWM = 4;

int backMotorDirection = 23;
int backMotorPWM = 3;

int rightMotorDirection = 22;
int rightMotorPWM = 2;

int speed = 200;
int speedForTopRight = speed;https://www.youtube.com/watch?v=RfDbaBYknqc
int adjustment = 10;
int speedForClockAndAntiClockMotion = 70;

//Pneumatics Control
//int pneumaticsExtend = 52;
//int pneumaticsContract = 53;




void setup()
{
  pinMode(frontMotorDirection, OUTPUT);
  pinMode(fronMotorPWM, OUTPUT);

  pinMode(leftMotorDirection, OUTPUT);
  pinMode(leftMotorPWM, OUTPUT);

  pinMode(backMotorDirection, OUTPUT);
  pinMode(backMotorPWM, OUTPUT);

  pinMode(rightMotorDirection, OUTPUT);
  pinMode(rightMotorPWM, OUTPUT);

  //pinMode(pneumaticsExtend, OUTPUT);
  //pinMode(pneumaticsContract, OUTPUT);

  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);

}


void loop()
{
  delay(100); // Wait for checking of transmission connection
}

void receiveEvent(int howMany)
{
    while(1 < Wire.available() ) // Necessary for reading accurate values from Due
    {

      char c  = Wire.read();
      Serial.println(c);
    }

    int x = Wire.read();
    Serial.print(x);



    if (x == 10)
    {
      upMovement();
      Serial.println("Moving Forward");
    }
    else if(x == 20)
    {
      leftMovement();
      Serial.println("Moving Left");
    }
    else if(x == 30)
    {
      downMovement();
      Serial.println("Moving Reverse");
    }
    else if(x == 40)
    {
      rightMovement();
      Serial.println("Moving Right");
    }
    else if(x == 50)
    {
      allStop();
      Serial.println("Stopping Everything");
    }
    else if (x == 60)
    {
      wholeBotAntiClockwise();
      Serial.println("antiClockwise Motion");
    }
    else if( x == 70)
    {
      wholeBotClockwise();
      Serial.println("Clockwise Motion");
    }
    else if(x == 100)
    {
      Serial.println("Moving Right while Following Right Side of Line sensor");
      rightMovementAdjustmentForRightSideOfLineSensor();
    }
    else if( x == 101 )
    {
      Serial.println("Moving Right while Following Left Side of Line sensor");
      rightMovementAdjustmentForLeftSideOfLineSensor();

    }
    /*else if( x == 75 )
    {
      Serial.println("Extending pneumatics");
      pneumaticsExtendControl();

    }
    else if( x == 76 )
    {
      Serial.println("Contracting pneumatics");
      pneumaticsContractControl();

    }*/
}

void upMovement()
{

    digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(fronMotorPWM, 0);

    digitalWrite(leftMotorDirection, HIGH);
    analogWrite(leftMotorPWM, speed);

    digitalWrite(backMotorDirection, LOW);
    analogWrite(backMotorPWM, 0);

    digitalWrite(rightMotorDirection, LOW);
    analogWrite(rightMotorPWM, speed);
}


void leftMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void downMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speed);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speed);



}


void rightMovement()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void allStop()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

  digitalWrite(pneumaticsExtend, LOW);
  digitalWrite(pneumaticsContract, LOW);



}


void wholeBotClockwise()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);


}

void wholeBotAntiClockwise()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);

}

void rightMovementAdjustmentForRightSideOfLineSensor( )
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed + adjustment);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed - adjustment);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);


}

void rightMovementAdjustmentForLeftSideOfLineSensor()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed + adjustment);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed - adjustment);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);
}
void pneumaticsExtendControl()
{
  digitalWrite(pneumaticsContract, LOW);
  digitalWrite(pneumaticsExtend, HIGH);
  //delay(200);
  //digitalWrite(pneumaticsExtend, LOW);
}

void pneumaticsContractControl()
{
  digitalWrite(pneumaticsExtend, LOW);
  digitalWrite(pneumaticsContract, HIGH);
  //delay(5);
  //digitalWrite(pneumaticsContract, LOW);
}
