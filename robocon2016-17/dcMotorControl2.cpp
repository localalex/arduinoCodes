int motorPin1 = 6;
int motorPin2 = 7;

void setup()
{
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);

}

void loop()
{
  for (int i = 0; i <250; i++)
  {
    analogWrite(motorPin1,i);
    analogWrite(motorPin2,0);
    delay(100);
  }
delay(5000);
  for(int i = 250; i > 0; i--)
  {
    analogWrite(motorPin1,i);
    analogWrite(motorPin2,0);
  }

  delay(500);
}
