/*
 Example sketch for the PS3 Bluetooth library - developed by Kristian Lauszus
 For more information visit my blog: http://blog.tkjelectronics.dk/ or
 send me an e-mail:  kristianl@tkjelectronics.com
 */

#include <PS3BT.h>
#include <usbhub.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

USB Usb;
//USBHub Hub1(&Usb); // Some dongles have a hub inside

BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
/* You can create the instance of the class in two ways */
PS3BT PS3(&Btd); // This will just create the instance
//PS3BT PS3(&Btd, 0x00, 0x15, 0x83, 0x3D, 0x0A, 0x57); // This will also store the bluetooth address - this can be obtained from the dongle when running the sketch

bool printTemperature, printAngle;

// Chasis motors arranged in 90 degree position.
int frontMotorDirection = 25;
int fronMotorPWM = 5;

int leftMotorDirection = 24;
int leftMotorPWM = 4;

int backMotorDirection = 23;
int backMotorPWM = 3;

int rightMotorDirection = 22;
int rightMotorPWM = 2;

// default speed of chasis, throwing and ACL & CL motors
int speed = 255;
int speedForClockAndAntiClockMotion = 50;

//Pin attached to the the pneumatic control of throwing piston.
int throwingPiston = 31;

//Pins of the throwing motor (Nischibo motors)
int throwingMotorPinA = 26 ;
int throwingMotorPinB = 27;
int throwingMotorPWM = 6;

int maxSpeedForThrowingMotor = 255;
int speedForThrowingMotor = 100;
int minSpeedForThrowingMotor = 0;

//Motor to control the X axis rotation( in a plane) of the throwing mechanism
int xAxisMotorPinA = 8;
int xAxisMotorPinB = 9;
//int xAxisMotorEnable = 47;

/*
int zAxisMotorPinA = ;
int zAxisMotorPinB = ;
int zAxisMotorEnable = ;

int verticalMechanismPullMotorPinA = ;
int verticalMechanismPullMotorPinB = ;
int verticalMechanismEnable = ;

*/

int selectButtonFlag = 0;


void setup()
{
  pinMode(frontMotorDirection, OUTPUT);
  pinMode(fronMotorPWM, OUTPUT);

  pinMode(leftMotorDirection, OUTPUT);
  pinMode(leftMotorPWM, OUTPUT);

  pinMode(backMotorDirection, OUTPUT);
  pinMode(backMotorPWM, OUTPUT);

  pinMode(rightMotorDirection, OUTPUT);
  pinMode(rightMotorPWM, OUTPUT);


  pinMode(throwingPiston, OUTPUT);

  pinMode(throwingMotorPinA, OUTPUT);
  pinMode(throwingMotorPinB, OUTPUT);
  pinMode(throwingMotorPWM, OUTPUT);

  pinMode(xAxisMotorPinA, OUTPUT);
  pinMode(xAxisMotorPinB, OUTPUT);
  //pinMode(xAxisMotorEnable, OUTPUT);

  /*
  pinMode(zAxisMotorPinA, OUTPUT);
  pinMode(zAxisMotorPinB, OUTPUT);
  pinMode(zAxisMotorEnable, OUTPUT);

  pinMode(verticalMechanismPullMotorPinA, OUTPUT);
  pinMode(verticalMechanismPullMotorPinB, OUTPUT);
  //pinMode(verticalMechanismEnable, OUTPUT);
  */


  Serial.begin(115200);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started"));
}
void loop() {
  Usb.Task();

  if (PS3.PS3Connected || PS3.PS3NavigationConnected) {
    if (PS3.getAnalogHat(LeftHatX) > 137 || PS3.getAnalogHat(LeftHatX) < 117 || PS3.getAnalogHat(LeftHatY) > 137 || PS3.getAnalogHat(LeftHatY) < 117 || PS3.getAnalogHat(RightHatX) > 137 || PS3.getAnalogHat(RightHatX) < 117 || PS3.getAnalogHat(RightHatY) > 137 || PS3.getAnalogHat(RightHatY) < 117) {
      Serial.print(F("\r\nLeftHatX: "));
      Serial.print(PS3.getAnalogHat(LeftHatX));
      Serial.print(F("\tLeftHatY: "));
      Serial.print(PS3.getAnalogHat(LeftHatY));
      if (PS3.PS3Connected) { // The Navigation controller only have one joystick
        Serial.print(F("\tRightHatX: "));
        Serial.print(PS3.getAnalogHat(RightHatX));
        Serial.print(F("\tRightHatY: "));
        Serial.print(PS3.getAnalogHat(RightHatY));
      }
    }

    // Analog button values can be read from almost all buttons
    /*if (PS3.getAnalogButton(L2) )
    {
      Serial.print(F("\r\nL2: "));
      Serial.print(PS3.getAnalogButton(L2));
    }

    if ( PS3.getAnalogButton(R2) )
    {
      if (PS3.PS3Connected)
      {
        Serial.print(F("\tR2: "));
        Serial.print(PS3.getAnalogButton(R2));
      }
    }*/

    if (PS3.getButtonClick(L2))
    {
      wholeBotAntiClockwise();
    }

    if(PS3.getButtonClick(R2))
    {
      wholeBotClockwise();
    }


    if (PS3.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
      //PS3.disconnect();
    }
    else {
      if (PS3.getButtonClick(TRIANGLE))
      {
        Serial.print(F("\r\nTriangle"));
        throwFrisbee();
      }
      if (PS3.getButtonClick(CIRCLE)) {
        Serial.print(F("\r\nCircle"));
        loadFrisbee();
      }
      if (PS3.getButtonClick(CROSS))
      {
        Serial.print(F("\r\nCross"));
        allStop();
      }
      if (PS3.getButtonClick(SQUARE))
        Serial.print(F("\r\nSquare"));

      if (PS3.getButtonClick(UP))
      {
        Serial.print(F("\r\nUp"));
        upMovement();
        if (PS3.PS3Connected)
        {
          PS3.setLedOff();
          PS3.setLedOn(LED4);
        }
      }
      if (PS3.getButtonClick(RIGHT))
      {
        Serial.print(F("\r\nRight"));
        rightMovement();
        if (PS3.PS3Connected)
        {
          PS3.setLedOff();
          PS3.setLedOn(LED1);
        }
      }
      if (PS3.getButtonClick(DOWN))
      {
        Serial.print(F("\r\nDown"));
        downMovement();
        if (PS3.PS3Connected)
        {
          PS3.setLedOff();
          PS3.setLedOn(LED2);
        }
      }
      if (PS3.getButtonClick(LEFT))
      {
        Serial.print(F("\r\nLeft"));
        leftMovement();
        if (PS3.PS3Connected)
        {
          PS3.setLedOff();
          PS3.setLedOn(LED3);
        }
      }


      if (PS3.getButtonClick(L3))
        Serial.print(F("\r\nL3"));

      if (PS3.getButtonClick(R3))
        Serial.print(F("\r\nR3"));

      if (PS3.getButtonClick(SELECT))
      {
        selectButtonFlag = 1;
        Serial.print(F("\r\nSelect - "));
        PS3.printStatusString();

      }
      if (PS3.getButtonClick(START)) {
        Serial.print(F("\r\nStart"));
        printAngle = !printAngle;
      }

      if(selectButtonFlag == 0)
      {
        if (PS3.getButtonClick(L1))
        {
          Serial.print(F("\r\nL1"));
          throwingMotorSpeedIncrease();
        }

        if (PS3.getButtonClick(R1))
        {
          Serial.print(F("\r\nR1"));
          throwingMotorSpeedDecrease();
        }
      }

      if(selectButtonFlag == 1)
      {
        if (PS3.getButtonClick(L1))
        {
          Serial.print(F("\r\nL1"));
          xAxisMotorNegativeTurn();
        }

        if (PS3.getButtonClick(R1))
        {
          Serial.print(F("\r\nR1"));
          xAxisMotorPositiveTurn();
        }
        //selectButtonFlag = 0;

      }
    }

#if 0 // Set this to 1 in order to see the angle of the controller
    if (printAngle) {
      Serial.print(F("\r\nPitch: "));
      Serial.print(PS3.getAngle(Pitch));
      Serial.print(F("\tRoll: "));
      Serial.print(PS3.getAngle(Roll));
    }
#endif
  }
}


void upMovement()
{

    digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(fronMotorPWM, 0);

    digitalWrite(leftMotorDirection, HIGH);
    analogWrite(leftMotorPWM, speed);

    digitalWrite(backMotorDirection, LOW);
    analogWrite(backMotorPWM, 0);

    digitalWrite(rightMotorDirection, LOW);
    analogWrite(rightMotorPWM, speed);
}


void leftMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void downMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speed);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speed);



}


void rightMovement()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void allStop()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

  digitalWrite(throwingMotorPinA, LOW);
  digitalWrite(throwingMotorPinB, LOW);
  digitalWrite(throwingMotorPWM, 0);

  selectButtonFlag = 0;

}


void wholeBotClockwise( )
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);


}

void wholeBotAntiClockwise( )
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speedForClockAndAntiClockMotion);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, speedForClockAndAntiClockMotion);

}

void throwFrisbee()
{
  digitalWrite(throwingPiston, HIGH);
}

void loadFrisbee()
{
  digitalWrite(throwingPiston, LOW);
}

void throwingMotorSpeedIncrease()
{
  digitalWrite(throwingMotorPinA, HIGH);
  digitalWrite(throwingMotorPinB, LOW);
  analogWrite(throwingMotorPWM, speedForThrowingMotor);
  speedForThrowingMotor = speedForThrowingMotor + 25;

  if (speedForThrowingMotor > maxSpeedForThrowingMotor)
  {
    speedForThrowingMotor = maxSpeedForThrowingMotor;
    Serial.println("Max possible speed is reached, the speed now is 255");
  }
  Serial.println(speedForThrowingMotor);
}

void throwingMotorSpeedDecrease()
{
  speedForThrowingMotor = speedForThrowingMotor - 25 ;
    if (speedForThrowingMotor < minSpeedForThrowingMotor)
  {
    speedForThrowingMotor = minSpeedForThrowingMotor;
    Serial.println("min possible speed is reached, the motors are now turned off");
  }
  digitalWrite(throwingMotorPinA, HIGH);
  digitalWrite(throwingMotorPinB, LOW);
  analogWrite(throwingMotorPWM, speedForThrowingMotor);
  Serial.println(speedForThrowingMotor);

}

void xAxisMotorNegativeTurn()
{
  //digitalWrite(xAxisMotorEnable, HIGH);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, HIGH);
  delay(20);
  //digitalWrite(xAxisMotorEnable, LOW);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, LOW);

}

void xAxisMotorPositiveTurn()
{
  //digitalWrite(xAxisMotorEnable, HIGH);
  digitalWrite(xAxisMotorPinA, HIGH);
  digitalWrite(xAxisMotorPinB, LOW);
  delay(20);
  //digitalWrite(xAxisMotorEnable, LOW);
  digitalWrite(xAxisMotorPinA, LOW);
  digitalWrite(xAxisMotorPinB, LOW);

}

/*
void zAxisAntiClockwiseTilt()
{
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, HIGH);
  delay(20);
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, LOW);
}


void zAxisClockwiseTilt()
{
  digitalWrite(zAxisMotorPinA, HIGH);
  digitalWrite(zAxisMotorPinB, LOW);
  delay(20);
  digitalWrite(zAxisMotorPinA, LOW);
  digitalWrite(zAxisMotorPinB, LOW);
}


void verticalMechanismPullUp()
{

}

void verticalMechanismPullDown()
{

}
*/
