#include <PS3BT.h>
#include <usbhub.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

USB Usb;
//USBHub Hub1(&Usb); // Some dongles have a hub inside

BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
/* You can create the instance of the class in two ways */
PS3BT PS3(&Btd); // This will just create the instance
//PS3BT PS3(&Btd, 0x00, 0x15, 0x83, 0x3D, 0x0A, 0x57); // This will also store the bluetooth address - this can be obtained from the dongle when running the sketch

bool printTemperature, printAngle;
int outputToMega = 24;


//Line Sensor Structure   // From the Front side moving towards RIGHT
//LEFT Side Sensor
// ll3 ll2 ll1 lcl lcr lr1 lr2 lr3

//RIGHT Side Sensor
//rl3 rl2 rl1 rcl rcr rr1 rr2 rr3

//Line sensor LeftSide Pins

int ll3 = 22;
int ll2 = 23;
int ll1 = 24;
int lcl = 25;
int lcr = 26;
int lr1 = 27;
int lr2 = 28;
int lr3 = 29;

//Line Sensor Right Side

int rl3 = 53;
int rl2 = 52;
int rl1 = 51;
int rcl = 50;
int rcr = 49;
int rr1 = 48;
int rr2 = 47;
int rr3 = 46;





void setup() {
  Serial.begin(115200);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started"));


  pinMode(outputToMega, OUTPUT);
  digitalWrite(outputToMega, LOW);


    //pinMode setup for Left Side Line Sensor

    pinMode(ll3, INPUT);
    pinMode(ll2, INPUT);
    pinMode(ll1, INPUT);
    pinMode(lcl, INPUT);
    pinMode(lcr, INPUT);
    pinMode(lr1, INPUT);
    pinMode(lr2, INPUT);
    pinMode(lr3, INPUT);


    //pinMode setup for Right Side Line Sensor

    pinMode(rl3, INPUT);
    pinMode(rl2, INPUT);
    pinMode(rl1, INPUT);
    pinMode(rcl, INPUT);
    pinMode(rcr, INPUT);
    pinMode(rr1, INPUT);
    pinMode(rr2, INPUT);
    pinMode(rr3, INPUT);

}
void loop() {
  Usb.Task();


  //readLineSensorLeftSide();
  readLineSensorRightSide();

  if (PS3.PS3Connected || PS3.PS3NavigationConnected) {
    if (PS3.getAnalogHat(LeftHatX) > 137 || PS3.getAnalogHat(LeftHatX) < 117 || PS3.getAnalogHat(LeftHatY) > 137 || PS3.getAnalogHat(LeftHatY) < 117 || PS3.getAnalogHat(RightHatX) > 137 || PS3.getAnalogHat(RightHatX) < 117 || PS3.getAnalogHat(RightHatY) > 137 || PS3.getAnalogHat(RightHatY) < 117) {
      Serial.print(F("\r\nLeftHatX: "));
      Serial.print(PS3.getAnalogHat(LeftHatX));
      Serial.print(F("\tLeftHatY: "));
      Serial.print(PS3.getAnalogHat(LeftHatY));
      if (PS3.PS3Connected) { // The Navigation controller only have one joystick
        Serial.print(F("\tRightHatX: "));
        Serial.print(PS3.getAnalogHat(RightHatX));
        Serial.print(F("\tRightHatY: "));
        Serial.print(PS3.getAnalogHat(RightHatY));
      }
    }

    // Analog button values can be read from almost all buttons
    if (PS3.getAnalogButton(L2) || PS3.getAnalogButton(R2)) {
      Serial.print(F("\r\nL2: "));
      Serial.print(PS3.getAnalogButton(L2));
      if (PS3.PS3Connected) {
        Serial.print(F("\tR2: "));
        Serial.print(PS3.getAnalogButton(R2));
      }
    }

    if (PS3.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
    //  PS3.disconnect();
    }
    else {
      if (PS3.getButtonPress(TRIANGLE)) {
        Serial.print(F("\r\nTraingle"));
        //PS3.setRumbleOn(RumbleLow);
        digitalWrite(outputToMega, LOW);
      }
      else{
        digitalWrite(outputToMega, LOW);
      }
      if (PS3.getButtonClick(CIRCLE)) {
        Serial.print(F("\r\nCircle"));
        //PS3.setRumbleOn(RumbleHigh);
      }
      if (PS3.getButtonClick(CROSS))
        Serial.print(F("\r\nCross"));
      if (PS3.getButtonClick(SQUARE))
        Serial.print(F("\r\nSquare"));

      if (PS3.getButtonClick(UP)) {
        Serial.print(F("\r\nUp"));
        if (PS3.PS3Connected) {
          PS3.setLedOff();
          PS3.setLedOn(LED4);
        }
      }
      if (PS3.getButtonClick(RIGHT)) {
        Serial.print(F("\r\nRight"));
        if (PS3.PS3Connected) {
          PS3.setLedOff();
          PS3.setLedOn(LED1);
        }
      }
      if (PS3.getButtonClick(DOWN)) {
        Serial.print(F("\r\nDown"));
        if (PS3.PS3Connected) {
          PS3.setLedOff();
          PS3.setLedOn(LED2);
        }
      }
      if (PS3.getButtonClick(LEFT)) {
        Serial.print(F("\r\nLeft"));
        if (PS3.PS3Connected) {
          PS3.setLedOff();
          PS3.setLedOn(LED3);
        }
      }

      if (PS3.getButtonClick(L1))
        Serial.print(F("\r\nL1"));
      if (PS3.getButtonClick(L3))
        Serial.print(F("\r\nL3"));
      if (PS3.getButtonClick(R1))
        Serial.print(F("\r\nR1"));
      if (PS3.getButtonClick(R3))
        Serial.print(F("\r\nR3"));

      if (PS3.getButtonClick(SELECT)) {
        Serial.print(F("\r\nSelect - "));
        PS3.printStatusString();
      }
      if (PS3.getButtonClick(START)) {
        Serial.print(F("\r\nStart"));
        printAngle = !printAngle;
      }
    }
#if 1 // Set this to 1 in order to see the angle of the controller
    if (printAngle) {
      Serial.print(F("\r\nPitch: "));
      Serial.print(PS3.getAngle(Pitch));
      Serial.print(F("\tRoll: "));
      Serial.print(PS3.getAngle(Roll));
    }
#endif
  }
#if 0 // Set this to 1 in order to enable support for the Playstation Move controller
  else if (PS3.PS3MoveConnected) {
    if (PS3.getAnalogButton(T)) {
      Serial.print(F("\r\nT: "));
      Serial.print(PS3.getAnalogButton(T));
    }
    if (PS3.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
      PS3.disconnect();
    }
    else {
      if (PS3.getButtonClick(SELECT)) {
        Serial.print(F("\r\nSelect"));
        printTemperature = !printTemperature;
      }
      if (PS3.getButtonClick(START)) {
        Serial.print(F("\r\nStart"));
        printAngle = !printAngle;
      }
      if (PS3.getButtonClick(TRIANGLE)) {
        Serial.print(F("\r\nTriangle"));
        PS3.moveSetBulb(Red);
      }
      if (PS3.getButtonClick(CIRCLE)) {
        Serial.print(F("\r\nCircle"));
        PS3.moveSetBulb(Green);
      }
      if (PS3.getButtonClick(SQUARE)) {
        Serial.print(F("\r\nSquare"));
        PS3.moveSetBulb(Blue);
      }
      if (PS3.getButtonClick(CROSS)) {
        Serial.print(F("\r\nCross"));
        PS3.moveSetBulb(Yellow);
      }
      if (PS3.getButtonClick(MOVE)) {
        PS3.moveSetBulb(Off);
        Serial.print(F("\r\nMove"));
        Serial.print(F(" - "));
        PS3.printStatusString();
      }
    }
    if (printAngle) {
      Serial.print(F("\r\nPitch: "));
      Serial.print(PS3.getAngle(Pitch));
      Serial.print(F("\tRoll: "));
      Serial.print(PS3.getAngle(Roll));
    }
    else if (printTemperature) {
      Serial.print(F("\r\nTemperature: "));
      Serial.print(PS3.getTemperature());
    }
  }
#endif
}

void readLineSensorLeftSide()
{
  Serial.println(' ');
  int LL3 = digitalRead(ll3);
  Serial.print(LL3);
  Serial.print(' ');

  int LL2 = digitalRead(ll2);
  Serial.print(LL2);
  Serial.print(' ');

  int LL1 = digitalRead(ll1);
  Serial.print(LL1);
  Serial.print(' ');

  int LCL = digitalRead(lcl);
  Serial.print(LCL);
  Serial.print(' ');

  int LCR = digitalRead(lcr);
  Serial.print(LCR);
  Serial.print(' ');

  int LR1 = digitalRead(lr1);
  Serial.print(LR1);
  Serial.print(' ');

  int LR2 = digitalRead(lr2);
  Serial.print(LR2);
  Serial.print(' ');

  int LR3 = digitalRead(lr3);
  Serial.print(LR3);
  Serial.println(' ');

}

void readLineSensorRightSide()
{
  Serial.println(' ');
  int RL3 = digitalRead(rl3);
  Serial.print(RL3);
  Serial.print(' ');

  int RL2 = digitalRead(rl2);
  Serial.print(RL2);
  Serial.print(' ');

  int RL1 = digitalRead(rl1);
  Serial.print(RL1);
  Serial.print(' ');

  int RCL = digitalRead(rcl);
  Serial.print(RCL);
  Serial.print(' ');

  int RCR = digitalRead(rcr);
  Serial.print(RCR);
  Serial.print(' ');

  int RR1 = digitalRead(rr1);
  Serial.print(RR1);
  Serial.print(' ');

  int RR2 = digitalRead(rr2);
  Serial.print(RR2);
  Serial.print(' ');

  int RR3 = digitalRead(rr3);
  Serial.print(RR3);
  Serial.println(' ');

}


void leftSideMovementWithLineSensor()
{


}

void rightSideMovementWithLineSensor()
{

}
