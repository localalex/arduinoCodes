#include <PS2X_lib.h>

int leftLED = 38;
int upLED = 36;
int rightLED = 40;
int downLED = 42;


PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you conect the controller,
//or call config_gamepad(pins) again after connecting the controller.
int error = 0;
byte vibrate = 0;

void setup()
{
  pinMode(leftLED,OUTPUT);
  pinMode(upLED,OUTPUT);
  pinMode(rightLED,OUTPUT);
  pinMode(downLED,OUTPUT);

  Serial.begin(57600);

  error = ps2x.config_gamepad(8,4,7,2);   //setup GamePad(clock 7, command 2, attention 6, data 1) pins, check for error

  if(error == 0)
  {
    Serial.println("Found Controller, configured successful");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Go to www.billporter.info for updates and to report bugs.");
  }

  else if(error == 1)
   Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

  else if(error == 2)
   Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

  ps2x.enableRumble();
  ps2x.enablePressures();

}

void loop()
{
  digitalWrite(leftLED,LOW);
  digitalWrite(upLED,LOW);
  digitalWrite(rightLED,LOW);
  digitalWrite(downLED,LOW);

  if(error != 0)
  return;

  ps2x.read_gamepad(false, vibrate);

  if(ps2x.Button(PSB_START))
       Serial.println("Start is being held");
  if(ps2x.Button(PSB_SELECT))
       Serial.println("Select is being held");

  
}
