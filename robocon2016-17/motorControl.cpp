int motorPin1 = 4;
int motorPin2 = 5;
int enable = 9;
void setup()
{
  pinMode(motorPin1,OUTPUT);
  pinMode(motorPin2,OUTPUT);
  pinMode(enable,HIGH);
}

void loop()
{

  digitalWrite(motorPin1,HIGH);
  digitalWrite(motorPin2,LOW);
}
