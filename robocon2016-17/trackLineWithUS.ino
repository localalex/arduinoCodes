

//program for track line follower using LSA08
#include <NewPing.h>

//line sensor

int sensor[]={52,53,50,51,48,49,46,47};

//ultrasonic sensor
int trigpinL = 28; // To detect left edge and move bot towards right
int echopinL = 29;
int trigpinR = 11;  // To detect right edge and move bot towards left
int echopinR = 12;

//chasis
int leftMotorFwd = 7;
int leftMotorRev = 8;
int En = 6;
int rightMotorFwd = 9;
int rightMotorRev = 10;

int sens[8];
int flag = 0;
int speed=130;
int i=0;
int count = 0;

void setup()
{
  for(i=0;i<8;i++)                //line Sensor
  {
      pinMode(sensor[i],INPUT);
  }

  pinMode(leftMotorFwd,OUTPUT);   //chasis Motor
  pinMode(leftMotorRev,OUTPUT);
  pinMode(rightMotorFwd,OUTPUT);
  pinMode(rightMotorRev,OUTPUT);
  digitalWrite(En,HIGH);


  pinMode(trigpinL, OUTPUT);      // Left Ultrasonic sensor
  pinMode(echopinL, INPUT);

  pinMode(trigpinR, OUTPUT);      // Right Ultrasonic Sensor
  pinMode(echopinR, INPUT);

  Serial.begin(9600);
}

void loop()
{

  //ultrasonic sensor
  long durationL,distanceL;
  digitalWrite(trigpinL,LOW);
  delayMicroseconds(2);
  digitalWrite(trigpinL,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigpinL,LOW);

  durationL=pulseIn(echopinL,HIGH);
  distanceL=(durationL/2)/29.1;
  Serial.print("cm");
  Serial.print(distanceL);


  long durationR,distanceR;
  digitalWrite(trigpinR,LOW);
  delayMicroseconds(2);
  digitalWrite(trigpinR,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigpinR,LOW);

  durationR=pulseIn(echopinR,HIGH);
  distanceR=(durationR/2)/29.1;

  Serial.print("cm");
  Serial.print(distanceR);

  //line sensor
  for(i=0;i<8;i++)
  {
      sens[i] = digitalRead(sensor[i]);
  }

  Serial.print("\n");

    for(i=0;i<8;i++)
  {
      Serial.print(sens[i]);
      Serial.print("-");
  }
  Serial.print("\n");


  if (distanceL == 11 && distanceR == 11)
  {
      lineSensor();
  }

  else if (distanceL < 11 )
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }

  else if (distanceR < 11)
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }
}



void lineSensor()
{

  //fwd (pin 3 && pin 4 reserved for center)
  if (
       sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 1 &&
       sens[4] == 1 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }


  //left Detect Condition 1
  else if (( sens[0] == 0  &&  sens[1] == 0  &&  sens[2] == 1)&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }

  //left Detect Condition 2
  else if (( sens[0] == 0  && (sens[1] == 1  &&  sens[2] == 1))&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }

  //left Detect Condition 3
  else if (( sens[0] == 1  &&  sens[1] == 1  &&  sens[2] == 1)&& ( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 0))
  {
      analogWrite(leftMotorFwd,speed);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
  }


  //right Detect Condition 1
  else if (( sens[5] == 0  &&  sens[6] == 0  &&  sens[7] == 1)&& (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }

  //right Detect Condition 2
  else if (( sens[5] == 0  && (sens[6] == 1  &&  sens[7] == 1))&& (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }

  //right Detect Condition 3
  else if (( sens[5] == 1  &&  sens[6] == 1  &&  sens[7] == 1) && (sens[0]==0  && sens[1]==0 && sens[2]==0 && sens[3]==0  && sens[4]==0))
  {
      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,speed);
      analogWrite(rightMotorRev,0);
  }


  //cross Section Detection
  else if (
            sens[0] == 1 && sens[1] == 1 &&  sens[2] == 1 && sens[3] == 1 &&
            sens[4] == 1 && sens[5] == 1 && sens[6] == 1 &&  sens[7] == 1
          )
  {
      Serial.println("cross-section:");
      count = count + 1;
      Serial.println(count);

      analogWrite(leftMotorFwd,0);
      analogWrite(leftMotorRev,0);
      analogWrite(rightMotorFwd,0);
      analogWrite(rightMotorRev,0);
      delay(5000);
      flag =1;
   }

   if(flag == 1)
   {
          analogWrite(leftMotorFwd,speed);
          analogWrite(leftMotorRev,0);
          analogWrite(rightMotorFwd,speed);
          analogWrite(rightMotorRev,0);
          flag = 0;
   }


   if(
            sens[0] == 0 && sens[1] == 0 &&  sens[2] == 0 && sens[3] == 0 &&
            sens[4] == 0 && sens[5] == 0 && sens[6] == 0 &&  sens[7] == 0
     )
   {
          analogWrite(leftMotorFwd,0);
          analogWrite(leftMotorRev,speed);
          analogWrite(rightMotorFwd,0);
          analogWrite(rightMotorRev,speed);
   }

}
