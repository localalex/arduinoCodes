#include <PS2X_lib.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(32, 31, 33, 34, 35, 36);//LiquidCrystal(rs, enable, d4, d5, d6, d7)
const int m1=4;
const int m2=5;
const int m3=6;
const int m4=7;

PS2X ps2x;

int error = 0; 
byte vibrate = 0;

void setup(){
   pinMode(6, OUTPUT); 
 pinMode(7, OUTPUT); 
  pinMode(4, OUTPUT); 
 pinMode(5, OUTPUT); 
   lcd.begin(16, 2);
 Serial.begin(57600);

  
 error = ps2x.config_gamepad(13,11,10,12);
 
 if(error == 0){
  // Serial.println("Found Controller, configured successful");
  // Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
 // Serial.println("holding L1 or R1 will print out the analog stick values.");
  //Serial.println("Go to www.billporter.info for updates and to report bugs.");
 }
   
  else if(error == 1)
   Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");
   
  else if(error == 2)
   Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
   
   //Serial.print(ps2x.Analog(1), HEX);
 
 
 ps2x.enableRumble(); 
 ps2x.enablePressures();  
 
}

void loop(){
       digitalWrite(6, LOW); 
       digitalWrite(7, LOW); 
       digitalWrite(4, LOW); 
       digitalWrite(5, LOW);

 if(error != 0)
  return; 
  
  ps2x.read_gamepad(false, vibrate); 
 lcd.setCursor(0, 0);
    vibrate = ps2x.Analog(PSAB_BLUE); 

    if (ps2x.Analog(PSS_LY)>128)
  {
    lcd.print(ps2x.Analog(PSS_LY), DEC);
    analogWrite(6, (ps2x.Analog(PSS_LY)-256)); 
    analogWrite(7, 0); 
      
  }
  
 if (ps2x.Analog(PSS_LY)<128)
  {
    lcd.print(ps2x.Analog(PSS_LY), DEC);
    analogWrite(6, 0); 
    analogWrite(7, 255-(ps2x.Analog(PSS_LY))); 
  }
  if (ps2x.Analog(PSS_LY)==128)
  {
     digitalWrite(6, LOW); 
       digitalWrite(7, LOW); 
  }
  if(ps2x.Analog(PSS_LY)==255)
  {
    analogWrite(7, 0); 
    analogWrite(6, 255);
  }
   if (ps2x.Analog(PSS_RY)>128)
  {
    lcd.print(ps2x.Analog(PSS_RY), DEC);
    analogWrite(5, (ps2x.Analog(PSS_RY)-256)); 
       analogWrite(4, 0); 
       
  }
  if (ps2x.Analog(PSS_RY)<128)
  {
    lcd.print(ps2x.Analog(PSS_RY), DEC);
    analogWrite(5, 0); 
    analogWrite(4, 255-(ps2x.Analog(PSS_RY))); 
  }
  if (ps2x.Analog(PSS_RY)==128)
  {
     digitalWrite(4, LOW); 
       digitalWrite(5, LOW); 
  }
  if(ps2x.Analog(PSS_RY)==255)
  {
    analogWrite(4, 0); 
    analogWrite(5, 255);

  }
  
 delay(50);
}