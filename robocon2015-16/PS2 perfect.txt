#include <PS2X_lib.h>
const int m1=9;
const int m2=10;
const int m3=3;
const int m4=11;
PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning 
//you must always either restart your Arduino after you conect the controller, 
//or call config_gamepad(pins) again after connecting the controller.
int error = 0; 
byte vibrate = 0;

void setup(){
   pinMode(9, OUTPUT); 
 pinMode(10, OUTPUT); 
  pinMode(3, OUTPUT); 
 pinMode(11, OUTPUT); 
 Serial.begin(57600);

  
 error = ps2x.config_gamepad(13,11,10,12);   //setup GamePad(clock, command, attention, data) pins, check for error
 
 if(error == 0){
   Serial.println("Found Controller, configured successful");
   Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
  Serial.println("holding L1 or R1 will print out the analog stick values.");
  Serial.println("Go to www.billporter.info for updates and to report bugs.");
 }
   
  else if(error == 1)
   Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");
   
  else if(error == 2)
   Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
   
   //Serial.print(ps2x.Analog(1), HEX);
 
 
 ps2x.enableRumble();              //enable rumble vibration motors
 ps2x.enablePressures();           //enable reading the pressure values from the buttons. 
  

  
}

void loop(){
       digitalWrite(9, LOW); 
       digitalWrite(10, LOW); 
       digitalWrite(3, LOW); 
       digitalWrite(11, LOW);
   /* You must Read Gamepad to get new values
   Read GamePad and set vibration values
   ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
   if you don't enable the rumble, use ps2x.read_gamepad(); with no values
   
   you should call this at least once a second
   */
 if(error != 0)
  return; 
  
  ps2x.read_gamepad(false, vibrate);          //read controller and set large motor to spin at 'vibrate' speed
  
  if(ps2x.Button(PSB_START))                   //will be TRUE as long as button is pressed
       Serial.println("Start is being held");
  if(ps2x.Button(PSB_SELECT))
       Serial.println("Select is being held");
       
       
   if(ps2x.Button(PSB_PAD_UP)) {         //will be TRUE as long as button is pressed
     digitalWrite(9, LOW); 
       digitalWrite(10, HIGH); 
       digitalWrite(3, HIGH); 
       digitalWrite(11, LOW); 
    }
    if(ps2x.Button(PSB_PAD_RIGHT)){
     digitalWrite(4, LOW); 
       digitalWrite(5, LOW); 
       digitalWrite(6, HIGH); 
       digitalWrite(7, LOW); 
    }
    if(ps2x.Button(PSB_PAD_LEFT)){
     digitalWrite(4, LOW); 
       digitalWrite(5, HIGH); 
       digitalWrite(6, LOW); 
       digitalWrite(7, LOW); 
    }
    if(ps2x.Button(PSB_PAD_DOWN)){
     digitalWrite(4, HIGH); 
       digitalWrite(5, LOW); 
       digitalWrite(6, LOW); 
       digitalWrite(7, HIGH); 
    }   

  
    vibrate = ps2x.Analog(PSAB_BLUE);        //this will set the large motor vibrate speed based on 
                                            //how hard you press the blue (X) button    
  
  if (ps2x.NewButtonState())               //will be TRUE if any button changes state (on to off, or off to on)
  {
   
     
       
      if(ps2x.Button(PSB_L3))
       Serial.println("L3 pressed");
      if(ps2x.Button(PSB_R3))
       Serial.println("R3 pressed");
      if(ps2x.Button(PSB_L2))
       Serial.println("L2 pressed");
      if(ps2x.Button(PSB_R2))
       Serial.println("R2 pressed");
      if(ps2x.Button(PSB_GREEN))
       Serial.println("Triangle pressed");
       
  }   
       
  
  if(ps2x.Button(PSB_RED))             //will be TRUE if button was JUST pressed
      {
          digitalWrite(4, HIGH); 
       digitalWrite(5, LOW); 
       digitalWrite(6, HIGH); 
       digitalWrite(7, LOW); 
      
      }
       
  if(ps2x.Button(PSB_PINK))             //will be TRUE if button was JUST released
      {
         digitalWrite(4, LOW); 
       digitalWrite(5, HIGH); 
       digitalWrite(6, LOW); 
       digitalWrite(7, HIGH); 
      }
  
  if(ps2x.NewButtonState(PSB_BLUE))            //will be TRUE if button was JUST pressed OR released
       Serial.println("X just changed");    
  
  
  if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) // print stick values if either is TRUE
  {
      Serial.print("Stick Values:");
      Serial.print(ps2x.Analog(PSS_LY), DEC); //Left stick, Y axis. Other options: LX, RY, RX  
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_LX), DEC); 
      Serial.print(",");
      Serial.print(ps2x.Analog(PSS_RY), DEC); 
      Serial.print(",");
      Serial.println(ps2x.Analog(PSS_RX), DEC); 
  } 
    
 delay(50);
     
 

     
}