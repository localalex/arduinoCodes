#define InA1            10
#define PWM1            11                    
#define InA2            6                    
#define PWM2            7      

#define encodPinA1      2                 
#define encodPinB1      3 


#define encodPinA2      21                 
#define encodPinB2      20                  



#define LOOPTIME        100                     // PID loop time
#define NUMREADINGS     10                      // samples for Amp average

int readings[NUMREADINGS];
unsigned long lastMilli = 0;                    // loop timing 
unsigned long lastMilliPrint = 0;               // loop timing
int speed_req = 150;                            // speed (Set Point)

int speed_req2= 150;                            // speed (Set Point)
int speed_act = 0;                              // speed (actual value)
int speed_act2 = 0;                              // speed (actual value)
int PWM_val2 = 0;  
int PWM_val = 0;  



volatile long count = 0;                        // rev counter
volatile long count2 = 0;                        // rev counter
float Kp =   .18;                                // PID proportional control Gain
float Kd =    0.001;                                // PID Derivitave control gain


void setup() {
  analogReference(EXTERNAL);                            // Current external ref is 3.3V
  Serial.begin(115600);
  pinMode(InA1, OUTPUT);
  pinMode(PWM1, OUTPUT);
  pinMode(InA2, OUTPUT);
  pinMode(PWM2, OUTPUT);

  pinMode(encodPinA1, INPUT); 
  pinMode(encodPinB1, INPUT); 
  
  pinMode(encodPinA2, INPUT); 
  pinMode(encodPinB2, INPUT); 
  digitalWrite(encodPinA1, HIGH);                      // turn on pullup resistor
  digitalWrite(encodPinB1, HIGH);
  
  digitalWrite(encodPinA2, HIGH);                      // turn on pullup resistor
 digitalWrite(encodPinB2, HIGH);
 
 
  attachInterrupt(1, rencoder, FALLING); 
  attachInterrupt(3, rencoder2, FALLING);
  
  
  for(int i=0; i<NUMREADINGS; i++)   readings[i] = 0;  // initialize readings to 0

 

}

void loop() {

  if((millis()-lastMilli) >= LOOPTIME)   {                                    // enter tmed loop
    lastMilli = millis();
    
    
      getMotorData2();  
    getMotorData();      
  
 
    PWM_val2= updatePid2(PWM_val2, speed_req, speed_act2);
      digitalWrite(InA2, LOW);
   analogWrite(PWM2, PWM_val2-5);         // send PWM to motor
    
    
       PWM_val= updatePid(PWM_val, speed_req, speed_act); 
          digitalWrite(InA1, LOW);
   analogWrite(PWM1, PWM_val);       

 
  }
}

void getMotorData()  {                                                        // calculate speed, volts and Amps
static long countAnt = 0;                                                   // last count
  speed_act = ((count - countAnt)*(60*(1000/LOOPTIME)))/(360);          // 16 pulses X 29 gear ratio = 464 counts per output shaft rev*********************************************
  countAnt = count;                  

}


void getMotorData2()  {                                                        // calculate speed, volts and Amps
static long countAnt2 = 0;                                                   // last count
  speed_act2 = ((count2 - countAnt2)*(60*(1000/LOOPTIME)))/(360);          // 16 pulses X 29 gear ratio = 464 counts per output shaft rev*********************************************
  countAnt2 = count2;                  

}

int updatePid(int command, int targetValue, int currentValue)   {             // compute PWM value
float pidTerm = 0;                                                            // PID correction
int error=0;                                  
static int last_error=0;                             
  error = abs(targetValue) - abs(currentValue); 
  pidTerm = (Kp * error) + (Kd * (error - last_error));                            
  last_error = error;
  return constrain(command + int(pidTerm), 0, 255);
}

int updatePid2(int command, int targetValue, int currentValue)   {             // compute PWM value
float pidTerm = 0;                                                            // PID correction
int error=0;                                  
static int last_error=0;                             
  error = abs(targetValue) - abs(currentValue); 
  pidTerm = (Kp * error) + (Kd * (error - last_error));                            
  last_error = error;
  return constrain(command + int(pidTerm), 0, 255);
}


void rencoder()  {                                    // pulse and direction, direct port reading to save cycles
  if (PINB & 0b00000001)    count++;                // if(digitalRead(encodPinB1)==HIGH)   count ++;
  else                      count--;                // if (digitalRead(encodPinB1)==LOW)   count --;
}



void rencoder2()  {                                    // pulse and direction, direct port reading to save cycles
  if (PINB & 0b00000001)    count2++;                // if(digitalRead(encodPinB1)==HIGH)   count ++;
  else                      count2--;                // if (digitalRead(encodPinB1)==LOW)   count --;
}
